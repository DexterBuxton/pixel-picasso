import SwiftUI

@main
struct PixelPicassoApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
